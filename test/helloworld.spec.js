const supertest = require('supertest')
const app = require('../server.js')
const request = supertest(app)

describe('GET /', function(done) {
  it('displays "Hello World!"', function(done) {
    request.get('/')
      .expect('Hello World!')
      .then(() => done())
      .catch(done)
  })
})
